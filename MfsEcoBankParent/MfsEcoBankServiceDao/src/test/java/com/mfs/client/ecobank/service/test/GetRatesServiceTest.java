package com.mfs.client.ecobank.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.ecobank.dto.GetRatesRequestDto;
import com.mfs.client.ecobank.dto.GetRatesResponseDto;
import com.mfs.client.ecobank.dto.HostHeaderInfo;
import com.mfs.client.ecobank.dto.Rate;
import com.mfs.client.ecobank.service.GetRatesService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class GetRatesServiceTest {

	@Autowired
	GetRatesService getRatesService;

	//Check For Success test.
	@Ignore
	@Test
	public void getRatesServiceTest() throws Exception {

		GetRatesRequestDto request = new GetRatesRequestDto();

		HostHeaderInfo headerInfo = new HostHeaderInfo();
		headerInfo.setPartnerId("");
		headerInfo.setRequestId("");
		headerInfo.setCountryCode("");

		request.setHostHeaderInfo(headerInfo);
		Rate rate = new Rate();
		rate.setAmount(111111);
		rate.setAmountType("");
		rate.setSendCountry("");
		rate.setSendCcy("");
		rate.setReceiveCountry("");
		rate.setReceiveCcy("");
		rate.setDeliveryMethod("");
		rate.setTranType("");
		request.setRateandfees(rate);

		GetRatesResponseDto responseDto = getRatesService.getRates(request);
		System.out.println(responseDto);
		Assert.assertEquals("SUCCESS", responseDto.getHostHeaderInfo().getResponseMessage());

	}
}

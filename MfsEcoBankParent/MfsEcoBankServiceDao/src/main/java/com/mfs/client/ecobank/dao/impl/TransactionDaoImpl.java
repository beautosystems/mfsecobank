package com.mfs.client.ecobank.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.ecobank.dao.TransactionDao;

@EnableTransactionManagement
@Repository("TransactionDao")
public class TransactionDaoImpl extends BaseDaoImpl implements TransactionDao{

	@Autowired
	SessionFactory sessionFactory;

	private static final Logger LOGGER = Logger.getLogger(TransactionDaoImpl.class);

	
	
}

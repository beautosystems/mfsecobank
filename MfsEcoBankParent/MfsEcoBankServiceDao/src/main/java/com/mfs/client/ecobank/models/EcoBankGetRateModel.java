package com.mfs.client.ecobank.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ecobank_get_rates")
public class EcoBankGetRateModel {

	@Id
	@GeneratedValue
	@Column(name = "get_rate_id")
	private int getRateId;

	@Column(name = "partner_id")
	private String partnerId;

	@Column(name = "country_code")
	private String countryCode;

	@Column(name = "request_id")
	private String requestId;

	@Column(name = "request_token")
	private String requestToken;

	@Column(name = "source_ip")
	private String sourceIp;

	@Column(name = "amount")
	private double amount;

	@Column(name = "amount_type")
	private String amountType;

	@Column(name = "send_country")
	private String sendCountry;

	@Column(name = "send_ccy")
	private String sendCcy;

	@Column(name = "receive_country")
	private String receiveCountry;

	@Column(name = "receive_ccy")
	private String receiveCcy;

	@Column(name = "delivery_method")
	private String deliveryMethod;

	@Column(name = "tran_type")
	private String tranType;

	@Column(name = "response_code")
	private String responseCode;

	@Column(name = "response_message")
	private String responseMessage;

	@Column(name = "send_bank_code")
	private String sendBankCode;

	@Column(name = "destination_country")
	private String destinationCountry;

	@Column(name = "destination_bank_code")
	private String destinationBankCode;

	@Column(name = "send_amount")
	private String sendAmount;

	@Column(name = "receive_amount")
	private String receiveAmount;

	@Column(name = "total_charge")
	private String totalCharge;

	@Column(name = "exch_rate")
	private String exchRate;

	@Column(name = "delivery_time_in_hour")
	private String deliveryTimeInHour;

	@Column(name = "charge_payer")
	private String chargePayer;

	@Column(name = "transfer_currency")
	private String transferCurrency;

	@Column(name = "date_logged")
	private Date dateLogged;

	public int getGetRateId() {
		return getRateId;
	}

	public void setGetRateId(int getRateId) {
		this.getRateId = getRateId;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getRequestToken() {
		return requestToken;
	}

	public void setRequestToken(String requestToken) {
		this.requestToken = requestToken;
	}

	public String getSourceIp() {
		return sourceIp;
	}

	public void setSourceIp(String sourceIp) {
		this.sourceIp = sourceIp;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getAmountType() {
		return amountType;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

	public String getSendCountry() {
		return sendCountry;
	}

	public void setSendCountry(String sendCountry) {
		this.sendCountry = sendCountry;
	}

	public String getSendCcy() {
		return sendCcy;
	}

	public void setSendCcy(String sendCcy) {
		this.sendCcy = sendCcy;
	}

	public String getReceiveCountry() {
		return receiveCountry;
	}

	public void setReceiveCountry(String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	public String getReceiveCcy() {
		return receiveCcy;
	}

	public void setReceiveCcy(String receiveCcy) {
		this.receiveCcy = receiveCcy;
	}

	public String getDeliveryMethod() {
		return deliveryMethod;
	}

	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public String getTranType() {
		return tranType;
	}

	public void setTranType(String tranType) {
		this.tranType = tranType;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getSendBankCode() {
		return sendBankCode;
	}

	public void setSendBankCode(String sendBankCode) {
		this.sendBankCode = sendBankCode;
	}

	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

	public String getDestinationBankCode() {
		return destinationBankCode;
	}

	public void setDestinationBankCode(String destinationBankCode) {
		this.destinationBankCode = destinationBankCode;
	}

	public String getSendAmount() {
		return sendAmount;
	}

	public void setSendAmount(String sendAmount) {
		this.sendAmount = sendAmount;
	}

	public String getReceiveAmount() {
		return receiveAmount;
	}

	public void setReceiveAmount(String receiveAmount) {
		this.receiveAmount = receiveAmount;
	}

	public String getTotalCharge() {
		return totalCharge;
	}

	public void setTotalCharge(String totalCharge) {
		this.totalCharge = totalCharge;
	}

	public String getExchRate() {
		return exchRate;
	}

	public void setExchRate(String exchRate) {
		this.exchRate = exchRate;
	}

	public String getDeliveryTimeInHour() {
		return deliveryTimeInHour;
	}

	public void setDeliveryTimeInHour(String deliveryTimeInHour) {
		this.deliveryTimeInHour = deliveryTimeInHour;
	}

	public String getChargePayer() {
		return chargePayer;
	}

	public void setChargePayer(String chargePayer) {
		this.chargePayer = chargePayer;
	}

	public String getTransferCurrency() {
		return transferCurrency;
	}

	public void setTransferCurrency(String transferCurrency) {
		this.transferCurrency = transferCurrency;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "EcoBankGetRateModel [getRateId=" + getRateId + ", partnerId=" + partnerId + ", countryCode="
				+ countryCode + ", requestId=" + requestId + ", requestToken=" + requestToken + ", sourceIp=" + sourceIp
				+ ", amount=" + amount + ", amountType=" + amountType + ", sendCountry=" + sendCountry + ", sendCcy="
				+ sendCcy + ", receiveCountry=" + receiveCountry + ", receiveCcy=" + receiveCcy + ", deliveryMethod="
				+ deliveryMethod + ", tranType=" + tranType + ", responseCode=" + responseCode + ", responseMessage="
				+ responseMessage + ", sendBankCode=" + sendBankCode + ", destinationCountry=" + destinationCountry
				+ ", destinationBankCode=" + destinationBankCode + ", sendAmount=" + sendAmount + ", receiveAmount="
				+ receiveAmount + ", totalCharge=" + totalCharge + ", exchRate=" + exchRate + ", deliveryTimeInHour="
				+ deliveryTimeInHour + ", chargePayer=" + chargePayer + ", transferCurrency=" + transferCurrency
				+ ", dateLogged=" + dateLogged + "]";
	}

}

package com.mfs.client.ecobank.service;

import com.mfs.client.ecobank.dto.GetRatesRequestDto;
import com.mfs.client.ecobank.dto.GetRatesResponseDto;

public interface GetRatesService {

	GetRatesResponseDto getRates(GetRatesRequestDto ratesRequestDto);

	
	
}

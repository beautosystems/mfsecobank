package com.mfs.client.ecobank.util;

public class CommonConstant {

	public static final String CONTENT_TYPE = "Content-Type";
	public static final String APPLICATION_JSON = "application/json";
	public static final String BASE_URL = "base_url";
	public static final String PORT = "port";

	// Get Rates Headers
	public static final String CLIENTID = "x-client-id";
	public static final String BEARER_CLIENTID = "fadc5e4191e043599238d01fd0d51cbc";
	public static final String CLIENT_SECRET = "x-client-secret";
	public static final String BEARER_CLIENT_SECRET = "ef5b01f399Cb434faE95a8134D0b2cAC";
	public static final String REQUEST_SOURCE_CODE = "x-request-source-code";
	public static final String SOURCE_CODE = "MFSAFRICA";
	public static final String REQUEST_TOKEN = "x-request-token";
	public static final String TOKEN = "EW7oqi6xSdamWVdkbSu6KU7wfk5ZwooLUyXguxHc9M0=";
	
	// Get Rate Url
	public static final String RATE_URL = "rate_url";
	public static final String RATE_ENDPOINT = "rate_endpoint";

	// Validations 
	public static final String INVALID_PARTNER_ID = "Invalid Partner ID field";
	public static final String INVALID_REQUEST_ID = "Invalid Request ID field";
	public static final String INVALID_COUNTRY_CODE = "Invalid Country Code field";
	public static final String INVALID_AMOUNT = "Invalid Amount field";
	public static final String INVALID_AMOUNT_TYPE = "Invalid Amount Type field";
	public static final String INVALID_SEND_COUNTRY = "Invalid Send Country field";
	public static final String INVALID_SEND_CCY = "Invalid Send Ccy field";
	public static final String INVALID_RECEIVE_COUNTRY = "Invalid Receive Country field";
	public static final String INVALID_RECEIVE_CCY = "Invalid Receive Ccy field";
	
}

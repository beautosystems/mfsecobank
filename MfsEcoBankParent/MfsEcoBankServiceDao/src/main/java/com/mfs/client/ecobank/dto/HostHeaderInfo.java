package com.mfs.client.ecobank.dto;

public class HostHeaderInfo {

	private String partnerId;
	private String countryCode;
	private String requestId;
	private String requestToken;
	private String sourceIp;

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getRequestToken() {
		return requestToken;
	}

	public void setRequestToken(String requestToken) {
		this.requestToken = requestToken;
	}

	public String getSourceIp() {
		return sourceIp;
	}

	public void setSourceIp(String sourceIp) {
		this.sourceIp = sourceIp;
	}

	@Override
	public String toString() {
		return "HostHeaderInfo [partnerId=" + partnerId + ", countryCode=" + countryCode + ", requestId=" + requestId
				+ ", requestToken=" + requestToken + ", sourceIp=" + sourceIp + "]";
	}

}

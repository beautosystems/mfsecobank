package com.mfs.client.ecobank.dao;

import java.util.Map;

public interface EcoBankSystemConfigDao {

	public Map<String, String> getConfigDetailsMap();

}

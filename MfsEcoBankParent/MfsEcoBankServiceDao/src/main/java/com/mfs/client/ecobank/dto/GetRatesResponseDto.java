package com.mfs.client.ecobank.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetRatesResponseDto {

	private HostHeaderInfo1 hostHeaderInfo;
	private Rate1 rateandfees;
	private String code;
	private String message;

	public HostHeaderInfo1 getHostHeaderInfo() {
		return hostHeaderInfo;
	}

	public void setHostHeaderInfo(HostHeaderInfo1 hostHeaderInfo) {
		this.hostHeaderInfo = hostHeaderInfo;
	}

	public Rate1 getRateandfees() {
		return rateandfees;
	}

	public void setRateandfees(Rate1 rateandfees) {
		this.rateandfees = rateandfees;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "GetRatesResponseDto [hostHeaderInfo=" + hostHeaderInfo + ", rateandfees=" + rateandfees + ", code="
				+ code + ", message=" + message + "]";
	}

}

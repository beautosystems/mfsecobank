package com.mfs.client.ecobank.dto;

public class GetRatesRequestDto {

	private HostHeaderInfo hostHeaderInfo;
	private Rate rateandfees;

	public HostHeaderInfo getHostHeaderInfo() {
		return hostHeaderInfo;
	}

	public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
		this.hostHeaderInfo = hostHeaderInfo;
	}

	public Rate getRateandfees() {
		return rateandfees;
	}

	public void setRateandfees(Rate rateandfees) {
		this.rateandfees = rateandfees;
	}

	@Override
	public String toString() {
		return "GetRatesRequestDto [hostHeaderInfo=" + hostHeaderInfo + ", rateandfees=" + rateandfees + "]";
	}

}

package com.mfs.client.ecobank.dto;

public class Rate {

	private double amount;
	private String amountType;
	private String sendCountry;
	private String sendCcy;
	private String receiveCountry;
	private String receiveCcy;
	private String deliveryMethod;
	private String tranType;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getAmountType() {
		return amountType;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

	public String getSendCountry() {
		return sendCountry;
	}

	public void setSendCountry(String sendCountry) {
		this.sendCountry = sendCountry;
	}

	public String getSendCcy() {
		return sendCcy;
	}

	public void setSendCcy(String sendCcy) {
		this.sendCcy = sendCcy;
	}

	public String getReceiveCountry() {
		return receiveCountry;
	}

	public void setReceiveCountry(String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	public String getReceiveCcy() {
		return receiveCcy;
	}

	public void setReceiveCcy(String receiveCcy) {
		this.receiveCcy = receiveCcy;
	}

	public String getDeliveryMethod() {
		return deliveryMethod;
	}

	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public String getTranType() {
		return tranType;
	}

	public void setTranType(String tranType) {
		this.tranType = tranType;
	}

	@Override
	public String toString() {
		return "Rate [amount=" + amount + ", amountType=" + amountType + ", sendCountry=" + sendCountry + ", sendCcy="
				+ sendCcy + ", receiveCountry=" + receiveCountry + ", receiveCcy=" + receiveCcy + ", deliveryMethod="
				+ deliveryMethod + ", tranType=" + tranType + "]";
	}

}

package com.mfs.client.ecobank.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.ecobank.dao.EcoBankSystemConfigDao;
import com.mfs.client.ecobank.models.EcoBankSystemConfigModel;


@EnableTransactionManagement
@Repository("EcoBankSystemConfigDao")
@Component
public class EcoBankSystemConfigDaoImpl implements EcoBankSystemConfigDao{

	private static final Logger LOGGER = Logger.getLogger(EcoBankSystemConfigDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@Transactional
	public Map<String, String> getConfigDetailsMap() {
		LOGGER.debug("Inside getConfigDetailsMap of EcoBankSystemConfigDaoImpl");

		Map<String, String> ecoBankSystemConfigurationMap = null;
		Session session = sessionFactory.getCurrentSession();

		String hql = " From EcoBankSystemConfigModel";
		Query query = session.createQuery(hql);
		List<EcoBankSystemConfigModel> systemConfigModel = query.list();

		if (systemConfigModel != null && !systemConfigModel.isEmpty()) {
			ecoBankSystemConfigurationMap = new HashMap<String, String>();
			for (EcoBankSystemConfigModel ecoBankSystemConfiguration : systemConfigModel) {
				ecoBankSystemConfigurationMap.put(ecoBankSystemConfiguration.getConfigKey(),
						ecoBankSystemConfiguration.getConfigValue());
			}
		}
		if (ecoBankSystemConfigurationMap != null && !ecoBankSystemConfigurationMap.isEmpty()) {
			LOGGER.debug("getConfigDetailsMap response size -> " + ecoBankSystemConfigurationMap.size());
		}
		return ecoBankSystemConfigurationMap;

	}

}

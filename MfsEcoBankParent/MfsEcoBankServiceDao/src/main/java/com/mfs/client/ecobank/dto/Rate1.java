package com.mfs.client.ecobank.dto;

public class Rate1 {

	private String sendCountry;
	private String sendBankCode;
	private String destinationCountry;
	private String destinationBankCode;
	private String sendAmount;
	private String receiveAmount;
	private String totalCharge;
	private String exchRate;
	private String amountType;
	private String deliveryTimeInHour;
	private String chargePayer;
	private String transferCurrency;

	public String getSendCountry() {
		return sendCountry;
	}

	public void setSendCountry(String sendCountry) {
		this.sendCountry = sendCountry;
	}

	public String getSendBankCode() {
		return sendBankCode;
	}

	public void setSendBankCode(String sendBankCode) {
		this.sendBankCode = sendBankCode;
	}

	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

	public String getDestinationBankCode() {
		return destinationBankCode;
	}

	public void setDestinationBankCode(String destinationBankCode) {
		this.destinationBankCode = destinationBankCode;
	}

	public String getSendAmount() {
		return sendAmount;
	}

	public void setSendAmount(String sendAmount) {
		this.sendAmount = sendAmount;
	}

	public String getReceiveAmount() {
		return receiveAmount;
	}

	public void setReceiveAmount(String receiveAmount) {
		this.receiveAmount = receiveAmount;
	}

	public String getTotalCharge() {
		return totalCharge;
	}

	public void setTotalCharge(String totalCharge) {
		this.totalCharge = totalCharge;
	}

	public String getExchRate() {
		return exchRate;
	}

	public void setExchRate(String exchRate) {
		this.exchRate = exchRate;
	}

	public String getAmountType() {
		return amountType;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

	public String getDeliveryTimeInHour() {
		return deliveryTimeInHour;
	}

	public void setDeliveryTimeInHour(String deliveryTimeInHour) {
		this.deliveryTimeInHour = deliveryTimeInHour;
	}

	public String getChargePayer() {
		return chargePayer;
	}

	public void setChargePayer(String chargePayer) {
		this.chargePayer = chargePayer;
	}

	public String getTransferCurrency() {
		return transferCurrency;
	}

	public void setTransferCurrency(String transferCurrency) {
		this.transferCurrency = transferCurrency;
	}

	@Override
	public String toString() {
		return "Rate1 [sendCountry=" + sendCountry + ", sendBankCode=" + sendBankCode + ", destinationCountry="
				+ destinationCountry + ", destinationBankCode=" + destinationBankCode + ", sendAmount=" + sendAmount
				+ ", receiveAmount=" + receiveAmount + ", totalCharge=" + totalCharge + ", exchRate=" + exchRate
				+ ", amountType=" + amountType + ", deliveryTimeInHour=" + deliveryTimeInHour + ", chargePayer="
				+ chargePayer + ", transferCurrency=" + transferCurrency + "]";
	}

}

package com.mfs.client.ecobank.util;

public enum ResponseCodes {

	ER204("ER204", "Exception while save data"),
	ER205("ER205", "Exception while update"),
	E401("E401", "MFS System Error "), 
	VALIDATION_ERROR("ER206", "Validation Error"),
	ER207("ER207","Request can not be null"),
	S200("200", "Success");
	
	
	
	private String code;
	private String message;

	private ResponseCodes(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}



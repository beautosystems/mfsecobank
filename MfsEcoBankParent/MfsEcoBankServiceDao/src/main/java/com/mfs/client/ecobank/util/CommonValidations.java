package com.mfs.client.ecobank.util;

public class CommonValidations {

	// Validation to check String Values Null or empty
		public boolean validateStringValues(String reqParamValue) {
			boolean validateResult = false;
			if (reqParamValue == null || reqParamValue.equals(""))
				validateResult = true;

			return validateResult;
		}

		// check for Integer Value null or not
		public boolean validateIntValues(Integer reqParamValue) {
			boolean validateResult = false;
			if (reqParamValue == null || reqParamValue.equals("") || reqParamValue == 0)
				validateResult = true;

			return validateResult;
		}

		// Validation to check Amount Values null
		public boolean validateAmountValues(Double amount) {
			boolean validateResult = false;
			if (amount == null  || amount==0)
				validateResult = true;

			return validateResult;
		}
}

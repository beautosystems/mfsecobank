package com.mfs.client.ecobank.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.ecobank.dao.EcoBankSystemConfigDao;
import com.mfs.client.ecobank.dao.TransactionDao;
import com.mfs.client.ecobank.dto.GetRatesRequestDto;
import com.mfs.client.ecobank.dto.GetRatesResponseDto;
import com.mfs.client.ecobank.exception.DaoException;
import com.mfs.client.ecobank.models.EcoBankGetRateModel;
import com.mfs.client.ecobank.service.GetRatesService;
import com.mfs.client.ecobank.util.CommonConstant;
import com.mfs.client.ecobank.util.EcoBankCodes;
import com.mfs.client.ecobank.util.HttpsConnectionRequest;
import com.mfs.client.ecobank.util.HttpsConnectionResponse;
import com.mfs.client.ecobank.util.HttpsConnectorImpl;
import com.mfs.client.ecobank.util.JSONToObjectConversion;
import com.mfs.client.ecobank.util.ResponseCodes;

@Service("GetRatesService")
public class GetRatesServiceImpl implements GetRatesService {

	private static final Logger LOGGER = Logger.getLogger(GetRatesServiceImpl.class);

	@Autowired
	EcoBankSystemConfigDao ecoBankSystemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	// Function to Get Rates
	public GetRatesResponseDto getRates(GetRatesRequestDto ratesRequestDto) {
		Map<String, String> systemConfigDetails = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectionResponse httpsConResult = null;
		Gson gson = new Gson();
		GetRatesResponseDto responseDto = new GetRatesResponseDto();
		LOGGER.info("GetRatesServiceImpl in getRates function request " + ratesRequestDto);

		try {

			// get Syste Config Data
			systemConfigDetails = ecoBankSystemConfigDao.getConfigDetailsMap();
			if (systemConfigDetails == null) {
				throw new Exception("No config Details Found");
			}


			// Set Header Parameters
			Map<String, String> headerMap = new HashMap<String, String>();
			headerMap.put(CommonConstant.CLIENTID, CommonConstant.BEARER_CLIENTID);
			headerMap.put(CommonConstant.CLIENT_SECRET, CommonConstant.BEARER_CLIENT_SECRET);
			headerMap.put(CommonConstant.REQUEST_SOURCE_CODE, CommonConstant.SOURCE_CODE);
			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
			headerMap.put(CommonConstant.REQUEST_TOKEN, CommonConstant.TOKEN);
			connectionRequest.setHeaders(headerMap);
			connectionRequest.setHttpmethodName("POST");
			connectionRequest.setPort(Integer.parseInt(systemConfigDetails.get(CommonConstant.PORT)));
			// Set URL for Get Rates
			connectionRequest.setServiceUrl(systemConfigDetails.get(CommonConstant.RATE_URL)
					+ systemConfigDetails.get(CommonConstant.RATE_ENDPOINT));

			// Get Response
			HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
			httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(gson.toJson(ratesRequestDto), connectionRequest);
			String str_result = httpsConResult.getResponseData();

			if (httpsConResult.getRespCode() == EcoBankCodes.S200.getCode()) {
				// convert Json to object
				responseDto = (GetRatesResponseDto) JSONToObjectConversion.getObjectFromJson(str_result,
						GetRatesResponseDto.class);
				LOGGER.info("GetRatesServiceImpl in getRates function response " + responseDto);
				// log get rates response
				logResponse(responseDto, ratesRequestDto);
			} else {
				responseDto.setCode(httpsConResult.getCode());
				responseDto.setMessage(httpsConResult.getTxMessage());
			}
			logResponse(responseDto, ratesRequestDto);
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in GetRatesServiceImpl" + de);
			responseDto.setCode(de.getStatus().getStatusCode());
			responseDto.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in GetRatesServiceImpl" + e);
			responseDto.setCode(ResponseCodes.E401.getCode());
			responseDto.setMessage(ResponseCodes.E401.getMessage());
		}

		return responseDto;
	}

	// Log get Rates
	private void logResponse(GetRatesResponseDto responseDto, GetRatesRequestDto ratesRequestDto) throws DaoException {

		EcoBankGetRateModel ecoBankGetRate = new EcoBankGetRateModel();

		ecoBankGetRate.setPartnerId(ratesRequestDto.getHostHeaderInfo().getPartnerId());
		ecoBankGetRate.setCountryCode(ratesRequestDto.getHostHeaderInfo().getCountryCode());
		ecoBankGetRate.setRequestId(ratesRequestDto.getHostHeaderInfo().getRequestId());
		ecoBankGetRate.setRequestToken(ratesRequestDto.getHostHeaderInfo().getRequestToken());
		ecoBankGetRate.setSourceIp(ratesRequestDto.getHostHeaderInfo().getSourceIp());
		ecoBankGetRate.setAmount(ratesRequestDto.getRateandfees().getAmount());
		ecoBankGetRate.setAmountType(ratesRequestDto.getRateandfees().getAmountType());
		ecoBankGetRate.setSendCountry(ratesRequestDto.getRateandfees().getSendCountry());
		ecoBankGetRate.setSendCcy(ratesRequestDto.getRateandfees().getSendCcy());
		ecoBankGetRate.setReceiveCountry(ratesRequestDto.getRateandfees().getReceiveCountry());
		ecoBankGetRate.setReceiveCcy(ratesRequestDto.getRateandfees().getReceiveCcy());
		ecoBankGetRate.setDeliveryMethod(ratesRequestDto.getRateandfees().getDeliveryMethod());
		ecoBankGetRate.setTranType(ratesRequestDto.getRateandfees().getTranType());

		ecoBankGetRate.setResponseCode(responseDto.getHostHeaderInfo().getResponseCode());
		ecoBankGetRate.setResponseMessage(responseDto.getHostHeaderInfo().getResponseMessage());
		ecoBankGetRate.setSendBankCode(responseDto.getRateandfees().getSendBankCode());
		ecoBankGetRate.setDestinationCountry(responseDto.getRateandfees().getDestinationCountry());
		ecoBankGetRate.setDestinationBankCode(responseDto.getRateandfees().getDestinationBankCode());
		ecoBankGetRate.setSendAmount(responseDto.getRateandfees().getSendAmount());
		ecoBankGetRate.setReceiveAmount(responseDto.getRateandfees().getReceiveAmount());
		ecoBankGetRate.setTotalCharge(responseDto.getRateandfees().getTotalCharge());
		ecoBankGetRate.setExchRate(responseDto.getRateandfees().getExchRate());
		ecoBankGetRate.setDeliveryTimeInHour(responseDto.getRateandfees().getDeliveryTimeInHour());
		ecoBankGetRate.setChargePayer(responseDto.getRateandfees().getChargePayer());
		ecoBankGetRate.setTransferCurrency(responseDto.getRateandfees().getTransferCurrency());
		ecoBankGetRate.setDateLogged(new Date());

		transactionDao.save(ecoBankGetRate);
	}
}

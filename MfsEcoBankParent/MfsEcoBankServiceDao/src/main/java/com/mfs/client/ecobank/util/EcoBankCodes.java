package com.mfs.client.ecobank.util;

public enum EcoBankCodes {

	S200(200 , "OK"),
	S201(201 , "Created"),
	S202(202 , "Accepted"),
	ER400(400 ,"Bad Request"),
	ER401(401 , "Unauthorized"),
	ER403(403 , "Forbidden"),
	ER404(404 , "Resource not found"),
	ER500(500 , "Internal server error"),
	ER503(503 , "Service Unavailable");
	
	private int code;
	private String message;

	
	private EcoBankCodes(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}


}

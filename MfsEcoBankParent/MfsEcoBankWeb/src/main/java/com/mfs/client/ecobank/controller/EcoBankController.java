package com.mfs.client.ecobank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.client.ecobank.dto.GetRatesRequestDto;
import com.mfs.client.ecobank.dto.GetRatesResponseDto;
import com.mfs.client.ecobank.service.GetRatesService;
import com.mfs.client.ecobank.util.CommonConstant;
import com.mfs.client.ecobank.util.CommonValidations;
import com.mfs.client.ecobank.util.ResponseCodes;

@RestController
public class EcoBankController {

	@Autowired
	GetRatesService getRatesService;

	@RequestMapping(value = "/test")
	public String test() {

		System.out.println("Success");
		return "Hello";

	}

	@RequestMapping(value = "/getRates", method = RequestMethod.POST)
	public GetRatesResponseDto getRates(@RequestBody GetRatesRequestDto ratesRequestDto) {

		GetRatesResponseDto responseDto = null;
		CommonValidations validation = new CommonValidations();

		// Check request is null or not
		if (ratesRequestDto != null) {
			if (validation.validateStringValues(ratesRequestDto.getHostHeaderInfo().getPartnerId())) {
				responseDto = new GetRatesResponseDto();
				responseDto.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				responseDto.setMessage(
						ResponseCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_PARTNER_ID);

			} else if (validation.validateStringValues(ratesRequestDto.getHostHeaderInfo().getRequestId())) {
				responseDto = new GetRatesResponseDto();
				responseDto.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				responseDto.setMessage(
						ResponseCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_REQUEST_ID);

			} else if (validation.validateStringValues(ratesRequestDto.getHostHeaderInfo().getCountryCode())) {
				responseDto = new GetRatesResponseDto();
				responseDto.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				responseDto.setMessage(
						ResponseCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_COUNTRY_CODE);

			} else if (validation.validateAmountValues(ratesRequestDto.getRateandfees().getAmount())) {
				responseDto = new GetRatesResponseDto();
				responseDto.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				responseDto
						.setMessage(ResponseCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_AMOUNT);

			} else if (validation.validateStringValues(ratesRequestDto.getRateandfees().getAmountType())) {
				responseDto = new GetRatesResponseDto();
				responseDto.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				responseDto.setMessage(
						ResponseCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_AMOUNT_TYPE);

			} else if (validation.validateStringValues(ratesRequestDto.getRateandfees().getSendCountry())) {
				responseDto = new GetRatesResponseDto();
				responseDto.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				responseDto.setMessage(
						ResponseCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_SEND_COUNTRY);

			} else if (validation.validateStringValues(ratesRequestDto.getRateandfees().getSendCcy())) {
				responseDto = new GetRatesResponseDto();
				responseDto.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				responseDto.setMessage(
						ResponseCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_SEND_CCY);

			} else if (validation.validateStringValues(ratesRequestDto.getRateandfees().getReceiveCountry())) {
				responseDto = new GetRatesResponseDto();
				responseDto.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				responseDto.setMessage(
						ResponseCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_RECEIVE_COUNTRY);

			} else if (validation.validateStringValues(ratesRequestDto.getRateandfees().getReceiveCcy())) {
				responseDto = new GetRatesResponseDto();
				responseDto.setCode(ResponseCodes.VALIDATION_ERROR.getCode());
				responseDto.setMessage(
						ResponseCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_RECEIVE_CCY);

			} else {
				responseDto = getRatesService.getRates(ratesRequestDto);
			}
		} else {
			responseDto = new GetRatesResponseDto();
			responseDto.setCode(ResponseCodes.ER207.getCode());
			responseDto.setCode(ResponseCodes.ER207.getMessage());
		}
		return responseDto;
	}
}
